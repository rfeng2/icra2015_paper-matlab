clear;
close all;

% Extract the SIFT keypoints and perform feature matching. SIFT and
% matching implementations are provided by David Lowe
imname1 = 'img1.ppm';
imname2 = 'img6.ppm';
[num, match, im1, im2, im1l, im2l] = match_dr(imname1, imname2);

% Concatenate the two images for visualization
im3 = appendimages(im1, im2);

% Find all the hypothetical match pairs
pmatch = [find(match); match(match ~= 0)]';

% Calculate the keypoint displacements for all these matches, in both x and
% y components
curr = [im2l(pmatch(:, 2), 2) - im1l(pmatch(:, 1), 2), im2l(pmatch(:, 2), 1) - im1l(pmatch(:, 1), 1)];

% Find the mode of the distribution of keypoint displacements
[ms, w] = find_mode_quick(curr, 5);

% Use the samples with sufficient weights (close enough to the mode) to
% perform parameter estimation of Laplacian distribution
sample = curr(w >= 0.5, :);
% s_mu = median(sample, 1);
% s_b = sum(abs(sample - repmat(s_mu, [size(sample, 1), 1])), 1) ./ size(sample, 1);
s_mu = mean(sample, 1);
s_b = std(sample, 0, 1);

% Calculate the precision and recall numbers. The threshold is the
% percentage of inliers that would like to be included in the matching
for j = 1 : 20
    INRATIO = 1 - 1 / 10^j;
    % Find the left and right boundary values to include a certain
    % percentage of inliers. The boundary values should be symmetrical with
    % respect to s_mu
%     [lx, ux] = getBoundary(s_mu(1), s_b(1), INRATIO);
    lx = s_mu(1) - (2 + (j - 1)/5)*s_b(1);
    ux = s_mu(1) + (2 + (j - 1)/5)*s_b(1);
    xcan = find(curr(:, 1) > lx & curr(:, 1) < ux);
    
%     [ly, uy] = getBoundary(s_mu(2), s_b(2), INRATIO);
    ly = s_mu(2) - (1 + (j - 1)/5)*s_b(2);
    uy = s_mu(2) + (1 + (j - 1)/5)*s_b(2);
    ycan = find(curr(:, 2) > ly & curr(:, 2) < uy);
    
    % The final candidates are the samples that fall within the rectangular
    % range of (lx, ux, ly, uy)
    common = intersect(xcan, ycan);
    mresult = pmatch(common, :);
    
    % Calculate the precision and recall
    switch i
        case 2
            gt = match1to2;
        case 3
            gt = match1to3;
        case 4
            gt = match1to4;
        case 5
            gt = match1to5;
        case 6
            gt = match1to6;
    end
    TP{j} = intersect(mresult, gt, 'rows');
    FP{j} = setdiff(mresult, gt, 'rows');
    FN{j} = setdiff(gt, mresult, 'rows');
    pre(j) = size(TP{j}, 1) / (size(TP{j}, 1) + size(FP{j}, 1));
    rec(j) = size(TP{j}, 1) / size(gt, 1);
end

% Visualization of the matching performance with best trade-off between
% precision and recall
% -- Cyan: true positve
% -- Red: false postive
% -- Green: false negative
[best, idx] = max((pre + rec)/2)
imshow(im3);
hold on;
cols1 = size(im1,2);
line([im1l(TP{idx}(:, 1),2)'; im2l(TP{idx}(:, 2),2)'+cols1], ...
    [im1l(TP{idx}(:, 1),1)'; im2l(TP{idx}(:, 2),1)'],'Color','c');
line([im1l(FP{idx}(:, 1),2)'; im2l(FP{idx}(:, 2),2)'+cols1], ...
    [im1l(FP{idx}(:, 1),1)'; im2l(FP{idx}(:, 2),1)'],'Color','r');
line([im1l(FN{idx}(:, 1),2)'; im2l(FN{idx}(:, 2),2)'+cols1], ...
    [im1l(FN{idx}(:, 1),1)'; im2l(FN{idx}(:, 2),1)'],'Color','g');
hold off;