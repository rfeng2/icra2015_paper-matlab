clear;
close all;

% Extract the SIFT keypoints and perform feature matching. SIFT and
% matching implementations are provided by David Lowe
imname1 = 'img1.ppm';
imname2 = 'img6.ppm';
[num, match, im1, im2, im1l, im2l] = match_dr(imname1, imname2);

% Concatenate the two images for visualization
im3 = appendimages(im1, im2);

% Find all the hypothetical match pairs
pmatch = [find(match); match(match ~= 0)]';

% Calculate the keypoint displacements for all these matches, in both x and
% y components
curr = [im2l(pmatch(:, 2), 2) - im1l(pmatch(:, 1), 2), im2l(pmatch(:, 2), 1) - im1l(pmatch(:, 1), 1)];

% Find the mode of the distribution of keypoint displacements
[ms, w] = find_mode_quick(curr, 5);

% Use the samples with sufficient weights (close enough to the mode) to
% perform parameter estimation of Laplacian distribution
sample = curr(w >= 0.5, :);
s_mu = median(sample, 1);
s_b = sum(abs(sample - repmat(s_mu, [size(sample, 1), 1])), 1) ./ size(sample, 1);

% Calculate the precision and recall numbers. The threshold is the
% percentage of inliers that would like to be included in the matching
INRATIO = 0.99;

% Find the left and right boundary values to include a certain
% percentage of inliers. The boundary values should be symmetrical with
% respect to s_mu
[lx, ux] = getBoundary(s_mu(1), s_b(1), INRATIO);
xcan = find(curr(:, 1) > lx & curr(:, 1) < ux);

[ly, uy] = getBoundary(s_mu(2), s_b(2), INRATIO);
ycan = find(curr(:, 2) > ly & curr(:, 2) < uy);

% The final candidates are the samples that fall within the rectangular
% range of (lx, ux, ly, uy)
common = intersect(xcan, ycan);
mresult = pmatch(common, :);

fprintf('Found %d matches.\n', size(mresult,1));

% Visulization
imshow(im3);
hold on;
cols1 = size(im1,2);
line([im1l(mresult(:, 1),2)'; im2l(mresult(:, 2),2)'+cols1], ...
    [im1l(mresult(:, 1),1)'; im2l(mresult(:, 2),1)'],'Color','c');
hold off;
