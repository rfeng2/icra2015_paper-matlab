function [ms, w] = find_mode_quick(displace, binwidth)

% Find the minimum and maximum displacements
low = min(displace, [], 1);
up = max(displace, [], 1);

% Decide the bin numbers based a given bin size
nbins = ceil([(up(1) - low(1)) / binwidth, (up(2) - low(2)) / binwidth]);

% Build a 3D histogram to capture the highest bin for good initialization
[h, hidx] = hist3(displace, nbins);
[row, col] = find(h == max(h(:)));
init = [hidx{1}(row(1)), hidx{2}(col(1))];

% Mode-seeking using mean shift
[ms, w] = meanshift(init, displace, 20);

end
