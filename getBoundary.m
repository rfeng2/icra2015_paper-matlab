% mu and b are the parameters of the Laplacian distribution
% p is the percentage of area that is covered between minb and maxb

% minb and maxb are the left and right boundary. They are symmetrical with
% respect to mu
function [minb, maxb] = getBoundary(mu, b, p)

maxb = -log(2 * (1-p)) * b + mu;
minb = 2 * mu - maxb;

end