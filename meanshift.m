% Mean Shift with Gaussian kernel
function [ms, w] = meanshift(init, x, h)

ms_pre = init;
[n, d] = size(x);

for i = 1 : 1000
    w = exp(-sum((repmat(ms_pre, [n, 1]) - x).^2, 2) / h.^2);
    ms = sum(x .* repmat(w, [1, d]), 1) / sum(w);
    if abs(ms_pre - ms) < 0.000001
        break;
    else
        ms_pre = ms;
    end
end

end